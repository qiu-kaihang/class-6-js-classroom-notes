# promise
在JavaScript中，`Promise`是一种用于处理异步操作的对象。它代表了一个尚未完成但最终会完成的操作，并且可以在操作完成时返回一个值或一个错误。

`Promise`对象有三种状态：

- `pending`（等待）：初始状态，表示操作尚未完成。
- `fulfilled`（已完成）：表示操作成功完成，可以获取到操作的结果。
- `rejected`（已拒绝）：表示操作失败，可以获取到操作失败的原因。

`Promise`对象有两个重要的方法：

- `then()`：用于处理操作成功完成的情况，接收一个回调函数作为参数。
- `catch()`：用于处理操作失败的情况，接收一个回调函数作为参数。

下面是一个使用`Promise`的示例：

```javascript
function fetchData() {
  return new Promise((resolve, reject) => {
    // 模拟异步操作
    setTimeout(() => {
      const data = { name: 'John', age: 30 };
      if (data) {
        resolve(data); // 操作成功完成
      } else {
        reject('Error fetching data'); // 操作失败
      }
    }, 2000);
  });
}

fetchData()
  .then(data => {
    console.log('Data fetched successfully:', data);
  })
  .catch(error => {
    console.error('Error fetching data:', error);
  });
```

在上面的代码中，`fetchData()`函数返回一个`Promise`对象，表示一个异步操作。在`Promise`的构造函数中，我们模拟了一个异步操作（使用`setTimeout`函数），并且在操作完成时调用了`resolve()`方法或`reject()`方法，表示操作成功完成或失败。

在调用`fetchData()`函数后，我们可以使用`.then()`方法来处理操作成功完成的情况，或者使用`.catch()`方法来处理操作失败的情况。在上面的示例中，我们使用`console.log()`函数来输出操作成功完成时的结果，或者使用`console.error()`函数来输出操作失败的原因。

使用`Promise`可以更好地处理异步操作，并且可以避免回调函数嵌套的问题。