# 23-11-08  js的函数
## 函数

 在 JavaScript 中，函数是一种重要的语言特性，以下是一些关于函数的知识点：

1. 定义函数：可以使用 `function` 关键字来定义一个函数。例如：
   ```javascript
   function greet(name) {
       console.log('Hello, ' + name);
   }
   ```

2. 调用函数：可以使用函数名和参数列表来调用函数。例如：
   ```javascript
   greet('Alice');
   ```

3. 函数参数：函数可以接受任意数量的参数，这些参数可以在函数定义中声明，也可以在调用函数时传递。例如：
   ```javascript
   function sum(a, b) {
       return a + b;
   }
   console.log(sum(2, 3)); // 输出: 5
   ```

4. 函数返回值：函数可以返回一个值，这个值可以是任何类型的值，包括基本类型和对象。例如：
   ```javascript
   function sum(a, b) {
       return a + b;
   }
   let result = sum(2, 3);
   console.log(result); // 输出: 5
   ```

5. 函数表达式：可以使用函数表达式来定义一个匿名函数，并将其赋值给一个变量。例如：
   ```javascript
   let greet = function(name) {
       console.log('Hello, ' + name);
   };
   greet('Alice');
   ```


函数是 JavaScript 中的重要组成部分，它们提供了一种简单而强大的方式来组织代码，并且可以用于实现各种功能。

## argument


  `arguments` 对象：在函数内部，可以使用 `arguments` 对象来访问所有传递给函数的参数，即使在函数定义中没有明确声明这些参数。`arguments` 对象是一个类数组对象，可以通过索引访问每个参数。例如：
   ```javascript
   function sum() {
       let total = 0;
       for (let i = 0; i < arguments.length; i++) {
           total += arguments[i];
       }
       return total;
   }
   console.log(sum(1, 2, 3)); // 输出: 6
   ```

## ...rest

 剩余参数(`...rest`)：在函数定义中，可以使用剩余参数(`...rest`)来表示接受任意数量的参数，并将这些参数作为一个数组来处理。例如：
   ```javascript
   function sum(...numbers) {
       return numbers.reduce((total, num) => total + num, 0);
   }
   console.log(sum(1, 2, 3, 4)); // 输出: 10
   ```

剩余参数(`...rest`)提供了一种更灵活的方式来处理函数的参数，而不需要使用 `arguments` 对象。它允许函数接受任意数量的参数，并将它们作为一个数组来处理，这在处理不确定数量的参数时非常有用。