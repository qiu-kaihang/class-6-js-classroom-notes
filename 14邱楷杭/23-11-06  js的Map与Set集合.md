# js的Map与Set集合
## Map
 
JavaScript中的Map是一种集合，用于存储键值对。以下是关于Map的一些知识点：

1. 创建Map：可以使用new关键字和Map构造函数来创建一个新的Map实例。例如：
   ```javascript
   let myMap = new Map();
   ```

2. 添加键值对：可以使用set()方法向Map中添加新的键值对。例如：
   ```javascript
   myMap.set('key1', 'value1');
   ```

3. 获取值：可以使用get()方法获取指定键对应的值。例如：
   ```javascript
   let value = myMap.get('key1');
   ```

4. 删除键值对：可以使用delete()方法删除指定的键值对。例如：
   ```javascript
   myMap.delete('key1');
   ```

5. 清空Map：可以使用clear()方法清空Map中的所有键值对。例如：
   ```javascript
   myMap.clear();
   ```

6. 遍历Map：可以使用for...of循环或者forEach()方法遍历Map中的键值对。例如：
   ```javascript
   for (let [key, value] of myMap) {
       console.log(key, value);
   }
   ```

7. Map的大小：可以使用size属性获取Map中键值对的数量。例如：
   ```javascript
   let size = myMap.size;
   ```

Map提供了一种灵活和高效的方式来存储和操作键值对，特别适用于需要快速查找和更新数据的场景。

## Set

JavaScript中的Set是一种集合，用于存储唯一值。以下是关于Set的一些知识点：

1. 创建Set：可以使用new关键字和Set构造函数来创建一个新的Set实例。例如：
   ```javascript
   let mySet = new Set();
   ```

2. 添加值：可以使用add()方法向Set中添加新的值。例如：
   ```javascript
   mySet.add('value1');
   ```

3. 删除值：可以使用delete()方法删除指定的值。例如：
   ```javascript
   mySet.delete('value1');
   ```

4. 清空Set：可以使用clear()方法清空Set中的所有值。例如：
   ```javascript
   mySet.clear();
   ```

5. 遍历Set：可以使用for...of循环或者forEach()方法遍历Set中的值。例如：
   ```javascript
   for (let value of mySet) {
       console.log(value);
   }
   ```

6. Set的大小：可以使用size属性获取Set中值的数量。例如：
   ```javascript
   let size = mySet.size;
   ```

7. 检查值是否存在：可以使用has()方法检查Set中是否包含指定的值。例如：
   ```javascript
   let hasValue = mySet.has('value1');
   ```

Set提供了一种简单而高效的方式来存储唯一值，特别适用于需要快速查找和去重的场景。